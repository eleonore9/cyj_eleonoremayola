import model


user1 = model.User.new("ele_the_rocker@gmail.com", "password", "EleRocks")
user2 = model.User.new("leo_for_johnny@yahoo.fr", "motdepasse", "Johnny4ever")

store1 = model.Store.new("T-shirt", 25, "/static/img/t-shirt_wolf_dreams.jpg")
store2 = model.Store.new("Pantalon", 45, "/static/img/leather_pants.jpeg")
store3 = model.Store.new("Blouson", 95, "/static/img/eagle_leather_jacket.jpg")
store4 = model.Store.new("Guitare", 115, "/static/img/guitare_skulls.jpg")
store5 = model.Store.new("Moto", 2995, "/static/img/harley_davidson_motorcycle.jpg")