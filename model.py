from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, ForeignKey, func
from sqlalchemy import Column, Integer, String, DateTime
import datetime
from sqlalchemy.orm import sessionmaker, relationship, backref, scoped_session

engine = create_engine("sqlite:///zerockersstore.db", echo=True)
session = scoped_session(sessionmaker(bind=engine,
									autocommit = False,
									autoflush = False))
Base = declarative_base()
Base.query = session.query_property()


#--------------------------------------------CLASSES definition:
#-------------------User Class----------------------------------
class User(Base):
	__tablename__ = "users"

	id = Column(Integer, primary_key = True)
	email = Column(String(80), nullable=False)
	password = Column(String(10), nullable=False)
	username = Column(String(20), nullable=True)

	def __init__(self, email, password, username):
		self.email = email
		self.password = password
		self.username = username

	#--------class methods--------------------
	@classmethod
	def authenticate(cls, email, password):
		auth = session.query(User).filter_by(email=email, password=password).first()
		return auth

	@classmethod
	def new(cls, email, password, username):
		user = User(email, password, username)
		session.add(user)
		session.commit()
		return user

	@classmethod
	def get(cls, id):
		user = session.query(User).get(id)
		return user

	@classmethod
	def check_name(cls, username):
		"""checks if a username is already used by another user."""
		check = session.query(User).filter_by(username=username).first()
		return check

	#--------instance methods------------------
	# def get_id(self, email, password, username):
	# 	"""retrieves the id of a particular user"""
	# 	user_id = session.query(User).filter_by(email=email, password=password, username=username)
	# 	return user_id

	def get_items(self, user_id):
		get_user_items = session.query(Item).filter_by(user_id=user_id).all()
		return get_user_items

	def get_name(self, user_id):
		"""get username from user id """
		username = session.query(User).filter_by(user_id=user_id).get(username=username)
		return username

	def change_password(self, id, new_password):
		user = session.query(User).get(id)
		user.password = new_password
		session.commit()
	
#-------------------Item Class----------------------------------
class Item(Base):
	__tablename__ = "items"

	id = Column(Integer, primary_key = True)
	item_name = Column(String(150), nullable=True)
	item_price = Column(Integer)
	bought_at = Column(DateTime(), nullable=True)
	item_img = Column(String(150))
	user_id = Column(Integer, ForeignKey('users.id'))

	user = relationship("User",
		backref=backref("items", order_by=id))

	def __init__(self, item_name, bought_at, item_price,item_img, user_id):
		self.item_name = item_name
		self.bought_at = bought_at
		self.item_price = item_price
		self.item_img = item_img
		self.user_id = user_id

	#--------class methods--------------------
	@classmethod
	def new(cls, item_name, bought_at, item_price, item_img, user_id):
		now = datetime.datetime.now()
		item = Item(None, item_name, now, bought_at, item_price, item_img, user_id)
		session.add(item)
		session.commit()
		return item

	@classmethod
	def get(cls, id):
		"""get Item using its id """
		item = session.query(Item).get(id)
		return item

	@classmethod
	def all(cls):
		items = session.query(Item).all()
		return items

	#--------instance methods------------------
	def delete_item(self, item_id):
		item = session.query(Item).get(item_id)
		item.item_name = None
		session.commit()

#-------------------Store Class----------------------------------
class Store(Base):
	__tablename__ = "stores"

	id = Column(Integer, primary_key = True)
	item_name = Column(String(150), nullable=True)
	item_price = Column(Integer)
	item_img = Column(String(150))
	item_id = Column(Integer, ForeignKey('items.id'))
	

	item = relationship("Item",
		backref=backref("stores", order_by=id))


	def __init__(self, item_name, item_price, item_img):
		self.item_name = item_name
		self.item_price = item_price
		self.item_img = item_img

	#--------class methods--------------------
	@classmethod
	def new(cls, item_name, item_price, item_img):
		store = Store(item_name, item_price, item_img)
		session.add(store)
		session.commit()
		return store
		
	@classmethod
	def get(cls, id):
		"""get Store Item using its id """
		store_item = session.query(Store).get(id)
		return store_item

	@classmethod
	def all(cls):
		store_items = session.query(Store).all()
		return store_items

	#--------instance methods------------------
	
	def delete_from_store(self, store_id):
		del_store = session.query(Store).filter_by(id=store_id)
		session.delete(del_store)
		session.commit()


#End of Classes definition



def main():
    """In case we need this for something"""
    pass

if __name__ == "__main__":
    main()