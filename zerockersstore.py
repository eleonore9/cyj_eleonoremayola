#!/home/eleonore/Documents/CYJ_EleonoreMayola/env/bin/python
# -*- coding=UTF-8 -*-
import os, sys
from flask import Flask, flash, render_template, redirect, request 
from flask import url_for, session, send_from_directory
import model
import datetime

app = Flask(__name__)
app.secret_key = 'this_is_my_not_so_secret_key'

@app.teardown_request
def teardown_request(exc):
	model.session.close()

#---------------to the store, to log in--------------------------
@app.route("/", methods=['POST', 'GET'])
def index():
	user_id = session.get("user_id", None)
	if user_id:
		user = model.User.get(user_id)
		username = user.username
		return render_template("user_store.html", username=username, user_id=user_id)
	return render_template("user_store.html")

@app.route("/login")
def login():
	return render_template("log_or_sign.html")

@app.route("/authenticate", methods=['POST'])
def authenticate():
	email = request.form['email']
	password = request.form['password']
	user_auth = model.User.authenticate(email, password)
	if user_auth is not None:
		session['user_id'] = user_auth.id
		flash('Bienvenue au paradis du rocker!')
		user_id = user_auth.id
		return redirect(url_for("index", user_id=user_id))
	return redirect(url_for("login"))

# @app.route("/sign_up")
# def sign_up():
#     return render_template("signup.html")

@app.route("/new_user", methods=['POST','GET'])
def new_user():
	email = request.form['email']
	password = request.form['password1']
	username = request.form['username']
	if model.User.authenticate(email, password) is not None:
		flash("Bah t'as déjà un compte dis donc!")
		flash("Ah que connecte toi avec ton pseudo et ton mot de passe.")
		return redirect(url_for("login"))
	
	if len(password) < 5 or request.form['password1'] != request.form['password2']:
		flash("Eh! retape ton mot de passe!")
		return redirect(url_for("login"))

	if model.User.check_name(username) is not None:
		flash("Ce pseudo est déjà utilisé. Ah que il t'en faut un autre!")
		return redirect(url_for("login"))

	new_user = model.User.new(email, password, username)
	if new_user is not None:
		session['user_id'] = new_user.id
		user_id = new_user.id
		return redirect(url_for("index", user_id=new_id))
	return redirect(url_for("login"))

#-----------------------User Page------------------------------------
@app.route("/basket/<int:user_id>")
def basket(user_id=None):
	user_id = session.get("user_id", None)
	user = model.User.get(user_id)
	username = user.username
	return render_template("basket.html", username=username)

#-------------------------Log out------------------------------------
@app.route("/logout")
def logout():
	session.pop('user_id', None)
	flash('Reviens vite!')
	return redirect(url_for('index'))


if __name__ == "__main__":
    # app.run(debug=True)

	app.run(debug=True, host="0.0.0.0")




